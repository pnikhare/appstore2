from django.contrib import admin
from apps.models import *

admin.site.register(Category)
admin.site.register(Screenshot)
admin.site.register(Author)
admin.site.register(Release)
admin.site.register(App)
